﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    float delta = 0.1f;
	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        float newZposition = transform.position.z - delta;
        transform.position = new Vector3(5.68f, 0.4f, newZposition);

        if(transform.position.z < -2.7)
        {
            delta = -0.1f;
        }
        else if(transform.position.z > 2.19)
        {
            delta = 0.1f;
        }
    }
}
