﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWorks : MonoBehaviour {
    //공의(다른 오브젝트) 위치를 확인
    GameObject ball;

	// Use this for initialization
	void Start () {
        ball = GameObject.Find("Ball");
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(ball.transform.position.x - 5,
            ball.transform.position.y + 2,
            ball.transform.position.z + 1);
	}
}
