﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {                 //ball이라는 클래스
    int count = 1;                                //멤버변수. 메소드가 아닌 클래스 안에 선언된 변수로 클래스 내 메소드에서 사용 가능
    float startingPoint;
    bool shouldPrintOver20 = true;
    bool shouldPrintOver30 = true;

    /*SphereCollider myCollider;*/

	// Use this for initialization
	void Start () {
        Debug.Log("Start");
        startingPoint = transform.position.x;

        /*myCollider = GetComponent<SphereCollider>();*/

        Rigidbody myRigidbody = GetComponent<Rigidbody>();
        Debug.Log("UseGravity?:" + myRigidbody.useGravity);
	}
	
	// Update is called once per frame
	void Update () {
        float distance;
        distance = transform.position.x - startingPoint;

        /*myCollider.radius += 0.01f;*/

        if (distance > 30)
        {
            if(shouldPrintOver30)
            {
                Debug.Log("Over 30 :" + distance);
                shouldPrintOver30 = false;
            }
                
        }
        else if (distance > 20)
        {
            if(shouldPrintOver20)
            {
                Debug.Log("Over 20 : " + distance);
                shouldPrintOver20 = false;
            }
        }
        /*
        else
        {
            Debug.Log("Less than 20 : " + distance);
        }
        */
	}
}
